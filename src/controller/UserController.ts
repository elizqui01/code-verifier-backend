import { Get, Delete,Post,Query, Route, Tags, Put } from "tsoa";
import { IUserController } from "./interfaces";
import { LogSuccess, LogError, LogWarning } from "../utils/logger";

//ORM - Users Collection
import { getAllUsers, getUserByID, deleteUserByID, updateUserById, getKatasFromUsers } from "../domain/orm/User.orm";

@Route("/api/users")
@Tags("UserController")

export class UserController implements IUserController {
    
    /**
     * Endpoint to retrieve the Users in the Collection "Users" of DB
     * @param {string} id Id of user to retrieve (optional)
     * @returns All users or user found by ID
    */
   @Get("/")
   public async getUsers(@Query()page: number, @Query()limit: number, @Query()id?: string): Promise<any> {
    
        let response: any = '';

        if(id){

            LogSuccess(`[/api/users] User by Id: ${id}`);
            response = await getUserByID(id);
        }else{
            LogSuccess(`[/api/users] Get All users Request`);
            response = await getAllUsers(page, limit);
        }
        
        return response;
    }
    /**
     * Endpoint to delete the Users in the Collection "Users" of DB
     * @param {string} id Id of user to delete (optional)
     * @returns message informing if deletion was correct
    */
    @Delete("/")
   public async deleteUser(@Query()id?: string): Promise<any> {
    
        let response: any = '';

        if(id){

            LogSuccess(`[/api/users] Delete by Id: ${id}`);
            await deleteUserByID(id).then((r) =>{
                response= {
                    message: `User with id ${id} deleted succesfully!`
                }
            });
        }else{
            LogWarning(`[/api/users] Delete User Request WITHOUT ID`);
            response = {
                message: 'Please, provide an ID to remove from database'
            }
        }
        
        return response;
    }
     
    @Put("/")
    public async updateUser(@Query()id:string, user: any): Promise<any> {

        let response: any = '';

        if(id){
            LogSuccess(`[/api/users] Update User By ID: ${id}`);
            await updateUserById(id, user).then((r) => {
                response = {
                    message: `User with id ${id} updated succesfully`
                }
            })
        }else
        {
            LogWarning('[api/users] Update user Request WITHOUT ID')
            response = {
                messagge: 'Please, provide an ID to update an existing user'
            }
        }

        return response;
    }
    @Get('/katas')
    public async getKatas(@Query()page: number, @Query()limit: number, @Query()id: string): Promise<any> {
        let response: any = '';

        response = await getKatasFromUsers(page, limit, id);
        
        return response;
    }
}
