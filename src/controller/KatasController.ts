import { Get, Delete,Post,Query, Route, Tags, Put } from "tsoa";
import { IKataController } from "./interfaces";
import { LogSuccess, LogError, LogWarning } from "../utils/logger";

//ORM - Users Collection
import { getAllKatas, getKataByID, deleteKataByID, updateKataById, createKata } from "../domain/orm/Kata.orm";
import { IKata } from "../domain/interfaces/IKata.interface";

@Route("/api/katas")
@Tags("KataController")

export class KatasController implements IKataController{
    
    /**
     * Endpoint to retrieve the Katas in the Collection "Kats" of DB
     * @param {string} id of Kata to retrieve (optional)
     * @returns All katas o kata found by ID
    */
    @Get("/")
    public async getKatas(@Query()page: number, @Query()limit: number, @Query()id?: string): Promise<any> {
    
        let response: any = '';

        if(id){

            LogSuccess(`[/api/katas] User by Id: ${id}`);
            response = await getKataByID(id);
        }else{
            LogSuccess(`[/api/katas] Get All users Request`);
            response = await getAllKatas(page, limit);
            //TODO: remove passwords from response
        }
        
        return response;
    }
    /**
     * Endpoint to delete the Katas in the Collection "Katas" of DB
     * @param {string} id of kata to delete (optional)
     * @returns message informing if deletion was correct
     * 
    */

    @Post("/")
    public async createKata(kata: IKata): Promise<any> {
        let response: any = '';

        if(kata)
        {
            LogSuccess(`[/api/auth] Create New User: ${kata.name}`);
            await createKata(kata).then((r) => {
                LogSuccess(`[/api/katas] Created Kata: ${kata.name}`);
                response = {
                    message: `Kata created successfully: ${kata.name}`
                }
            })
        }
        else
        {
            LogWarning('[/api/katas] Register needs Kata Entity')
            response = {
                message: 'Kata not Registered: Please, provide a Kata Entity to create ones'
            }
        }
        return response;
    }

    @Delete("/")
   public async deleteKata(@Query()id?: string): Promise<any> {
    
        let response: any = '';

        if(id){

            LogSuccess(`[/api/katas] Delete by Id: ${id}`);
            await deleteKataByID(id).then((r) =>{
                response= {
                    message: `Kata with id ${id} deleted succesfully!`
                }
            });
        }else{
            LogWarning(`[/api/katas] Delete Kata Request WITHOUT ID`);
            response = {
                message: 'Please, provide an ID to remove from database'
            }
        }
        
        return response;
    }
     
    @Put("/")
    public async updateKata(@Query()id:string, kata: any): Promise<any> {

        let response: any = '';

        if(id){
            LogSuccess(`[/api/katas] Update User By ID: ${id}`);
            await updateKataById(id, kata).then((r) => {
                response = {
                    message: `Kata with id ${id} updated succesfully`
                }
            })
        }else
        {
            LogWarning('[api/katas] Update user Request WITHOUT ID')
            response = {
                messagge: 'Please, provide an ID to update an existing kata'
            }
        }

        return response;
    }
}