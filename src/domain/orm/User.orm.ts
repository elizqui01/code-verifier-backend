import {userEntity} from "../entities/User.entity";

import { LogSuccess, LogError } from "../../utils/logger";
import { IUser } from "../interfaces/IUser.interface";
import { IAuth } from "../interfaces/IAuth.interface";
import { IKata } from "../interfaces/IKata.interface";

// Environment variables
import dotenv from 'dotenv';

//BCRYPT to manage the passwords
import bcrypt from 'bcrypt'

//JWT
import jwt from "jsonwebtoken";
import { response } from "express";
import { kataEntity } from "../entities/Kata.entity";
import mongoose, { mongo } from "mongoose";

// Configuration of environment variables
dotenv.config();

//Obtain Secret key to generate JWT
const secret = process.env.SECRETKEY || 'MYSECRETKEY';

//CRUD
/**
 * Method to obtain all Users from Collection "Users" in MOngo Server
 */
export const getAllUsers = async (page: number, limit: number): Promise<any[] | undefined> =>{
    try{
        let userModel = userEntity();

        let response: any = {};

        //Search all users (using pagination)
        await userModel.find()
        
        .select('name email age katas')
        .limit(limit)
        .skip(((page - 1)) * limit)
        .exec().then((users: IUser[]) =>{
                response.users = users;
        })

        //Count total documents in collection "Users"
        await userModel.countDocuments().then((total: number) => {
            response.totalPages = Math.ceil(total / limit);
            response.currentPage = page;
        });
        return response;
                //inicio probando users from console
                userModel.find()
                .then((userModel) => {
                    console.log('Usuarios encontrados:', userModel);
                })
                .catch((error) => {
                    console.error('Error al consultar usuarios:', error);
                }); 
                //fin
    }catch(error){
        LogError(`[ORM ERROR]: Getting All Users: ${error}`)
    }
}

// -Get User By ID
export const getUserByID = async (id: string): Promise<any | undefined> => {
    try{
        let userModel = userEntity();

        //Search User By ID
        return await userModel.findById(id).select('name email age katas');
    }
    catch(error){
        LogError(`[ORM ERROR]: Getting User By ID: ${error}`)
    }
}
// -Delete User By ID
export const deleteUserByID = async (id: string): Promise<any | undefined> =>{
    try {
        let userModel = userEntity();

        //Delete User by ID
        return await userModel.deleteOne({_id: id})
        
    } catch (error) {
        LogError(`[ORM ERROR]: Deleting User By ID: ${error}`);
    }
}
/*
// -Create new User 
export const createUser = async (user: any): Promise<any| undefined> =>{
    try {
        let userModel = userEntity();

        //Create / Insert new user
        return await userModel.create(user);
    } catch (error) {
        LogError(`[ORM ERROR]: Creating User: ${error}`);
    }
}
*/
// -Update User By ID
export const updateUserById = async (id: string, user: any): Promise<any | undefined> => {
    try {
        let userModel = userEntity();

        // Update User
        //return await userModel.updateOne({_id: id})
        return await userModel.findByIdAndUpdate(id, user)
    } catch (error) {
        LogError(`[ORM ERROR]: Updating User ${id}`)
    }
}

// Register User
export const registerUser = async (user: IUser): Promise<any | undefined> => {
    try {
        let userModel = userEntity();

        //Create / Insert new user
        return await userModel.create(user);
    } catch (error) {
        LogError(`[ORM ERROR]: Creating User: ${error}`);
    }
    
}
//Login User
export const loginUser = async (auth: IAuth): Promise<any | undefined> => {
    try {
        let userModel = userEntity();

        let userFound: IUser | undefined = undefined;
        let token = undefined;

        // Check if user exists by  Unique Email
        await userModel.findOne({email: auth.email}).then((user: IUser) =>{
            userFound = user;
        }).catch((error) => {
            console.error(`[ERROR Authentication in ORM]: User Not Found`);
            throw new Error(`[ERROR Authentication in ORM]: User Not Found: ${error}`)
        });

        // Check if Password is Valid (compare with bcrypt)
        let validPassword = bcrypt.compareSync(auth.password, userFound!.password);

        if(!validPassword){
            console.error(`[ERROR Authentication in ORM]: Password Not Valid`);
            throw new Error(`[ERROR Authentication in ORM]: Password Not Valid`)
        }
        // Generate our JWT
        token = jwt.sign({email: userFound!.email}, secret,{
            expiresIn: "2h"
        })

        return {
            user: userFound,
            token: token
        }
        //Find User by email
        /*userModel.findOne({email: auth.email},(err: any, user: IUser) => {
            if(err){
                // TODO: return ERROR => ERROR while searching
            }
            if(!user){
                // TODO: return ERROR => ERROR while searching
            }

            // Use Bcrypt to Compare Passwords
            let validPassword = bcrypt.compareSync(auth.password, user.password);

            if(!validPassword){
                //TODO: --> NOT AUTHORIZED (401)
            }

            //Create JWT
            let token = jwt.sign({email: user.email}, 'MYSECRETWORD',{
                expiresIn: "2h"
            })

            return token;
        })*/

    } catch (error) {
        LogError(`[ORM ERROR]: Creating User: ${error}`);
    }
}
//Logout User
export const logoutUser = async (): Promise<any | undefined> => {
    //TODO: NOT IMPLEMENTED
}

export const getKatasFromUsers = async (page: number, limit: number, id:string): Promise<any[] | undefined> =>{
    try{
        let userModel = userEntity();
        let katasModel = kataEntity();
        let katasFound: IKata[] = [];

        let response: any = {
            katas: []
        };

        await userModel.findById(id).then(async(user: IUser) => {
            //response.user.name = user.name;
            //response.user.email = user.email;

            // Create types to search
            let objectIds:mongoose.Types.ObjectId[] = [];
            user.katas.forEach((kataID: string) =>{
                let objectID = new mongoose.Types.ObjectId(kataID);
                objectIds.push(objectID);
            })

            await katasModel.find({"_id": {"$in": objectIds}}).then((katas: IKata[]) =>{
                katasFound = katas;
            })
        }).catch((error) =>{
            LogError(`[ORM ERROR]: Obtaining User: ${error}`);
        })

        return response.katas = katasFound; 
    }catch(error){
        LogError(`[ORM ERROR]: Getting All Users: ${error}`)
    }
}