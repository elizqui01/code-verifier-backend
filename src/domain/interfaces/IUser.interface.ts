import { IKata } from "./IKata.interface";

export interface IUser {
    name: string,
    lastname: string,
    email: string,
    password: string,
    age: Number,
    katas: string[]
}