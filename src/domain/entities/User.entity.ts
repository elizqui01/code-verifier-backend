import mongoose from "mongoose";
import { IUser } from "../interfaces/IUser.interface";

export const userEntity = () => {
    
    let userSchema = new mongoose.Schema<IUser>(
        {
            name: {type: String, required: true},
            lastname: {type: String, required: false},
            email: {type: String, required: true},
            password: {type: String, required: true},
            age: {type: Number, required: true},
            katas: {type: [], required: true}            
        }
    )
    return mongoose.models.Users || mongoose.model('Users', userSchema);
}