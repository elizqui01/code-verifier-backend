import express, { Request, Response } from "express";
import { UserController } from "../controller/UserController";
import { LogInfo } from "../utils/logger";



//Body Parser to read BODY from requests
import bodyParser from "body-parser";

let jsonParser = bodyParser.json();

//JWT Verifier Middleware
import { verifyToken } from "../domain/middlewares/verifyToken.middleware";

//Router from express
let usersRouter = express.Router();

//GET -> http://localhost:8000/api/users?id=64a4fad4c778e9126177c3c7
usersRouter.route('/')
    .get(verifyToken, async(req: Request, res: Response) =>{
        //Obtain a Query Param (ID)
        let id: any = req?.query?.id;

        //Pagination
        let page: any = req?.query?.page || 1;
        let limit: any = req?.query?.limit || 10;

        LogInfo(`Query Param: ${id}`);
        //Controller Instance to escute method
        const controller: UserController = new UserController();
        //Obtain Response
        const response: any = await controller.getUsers(page, limit, id);
        // Send to the client the response
        return res.status(200).send(response);
    })
    //DELETE
    .delete(verifyToken,async(req:Request, res: Response) =>{
        //Obtain a Query Param (ID)
        let id: any = req?.query?.id;
        LogInfo(`Query Param: ${id}`);
        //Controller Instance to escute method
        const controller: UserController = new UserController();
        //Obtain Response
        const response: any = await controller.deleteUser(id);
        //Send to the client the response
        return res.status(200).send(response);

    })
    //PUT
    .put(verifyToken,async(req:Request, res: Response) =>{
        //Obtain a Query Param (ID)
        let id: any = req?.query?.id;
        let name: any = req?.query?.name;
        let age: any = req?.query?.age;
        let email: any = req?.query?.email;
        LogInfo(`Query Param: ${id}`);

       
        //Controller Instance to escute method
        const controller: UserController = new UserController();
        
        let user = {
            name: name,
            email: email,
            age: age
        }
        //Obtain Response
        const response: any = await controller.updateUser(id, user);
        //Send to the client the response
        return res.status(200).send(response);
    })

    //
    usersRouter.route('/katas')
    .get(verifyToken, async(req: Request, res: Response) =>{
        //Obtain a Query Param (ID)
        let id: any = req?.query?.id;

        //Pagination
        let page: any = req?.query?.page || 1;
        let limit: any = req?.query?.limit || 10;

        //Controller Instance to escute method
        const controller: UserController = new UserController();
        //Obtain Response
        const response: any = await controller.getKatas(page, limit, id);
        // Send to the client the response
        return res.status(200).send(response);

    })
//Export Hello Router
export default usersRouter;
/**
 * l
 * Get Documents => 200 O
 * Creatin Documents => 201 OK
 * Deletion of Documents => 200 (Entity) / 204 (No return)
 * Update of Documents => 200 (Entity) / 204 (No return)
 * 
 */