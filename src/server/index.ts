import express, {Express, Request, Response} from "express";

// Swagger
import swaggerUi from 'swagger-ui-express';

//Envorionment Variables
import dotenv from 'dotenv';

//Security
import cors from 'cors';
import helmet from 'helmet';

//TODO: HTTPS

//Root Router
import rootRuter from '../routes'

import { getAllUsers, getUserByID } from "../domain/orm/User.orm";
import { userEntity } from "../domain/entities/User.entity";

const mongoose = require('mongoose')
/*
// Configuration the .env file
dotenv.config();
*/
// Create Express APP
const server: Express = express();

const dbUrl = 'mongodb://localhost:27017/codeverification';

// *Swagger config and route
server.use(
    '/docs',
    swaggerUi.serve,
    swaggerUi.setup(undefined,{
        swaggerOptions:{
            url: "/swagger.json",
            explorer: true
        }
    })
)

//Define SERVER to use "/api" and use rootRouter from 'index.ts' in routes
//From this point onover: http://localhost:8000/api/...
server.use(
    '/api',
    rootRuter
);

// Static Server
server.use(express.static('public'));
//TODO: Mongoose Connection
mongoose.connect(dbUrl, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    console.log('Conexión exitosa a MongoDB');
    // Aquí puedes empezar a realizar operaciones con la base de datos
    getAllUsers;
    /*let User = userEntity();
    User.find()
      .then((users) => {
        console.log('Usuarios encontrados:', users);
      })
      .catch((error) => {
        console.error('Error al consultar usuarios:', error);
      }); */
  })
  .catch(() => {
    console.error('Error al conectar a MongoDB:');
  });
/*
if (mongoose.connection.readyState === 1) {
    console.log('Conectado a MongoDB');
  } else {
    console.log('No conectado a MongoDB');
  }
*/

//Security Config
server.use(helmet());
server.use(cors());

//Content Type Config
server.use(express.urlencoded({extended: true, limit: '50mb'}));
server.use(express.json({limit: '50mb'}));

//http://localhost:800/ -->http://localhost:8000/api/
server.get('/',(req:Request, res: Response) =>{
    res.redirect('/api');
});

export default server;
